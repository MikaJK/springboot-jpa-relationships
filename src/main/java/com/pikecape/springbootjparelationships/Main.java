/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pikecape.springbootjparelationships;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Component;

/**
 *
 * @author Mika J. Korpela
 */
@SpringBootApplication
public class Main
{
    @Autowired
    static BookRepository bookRepository;
    
    @Autowired
    static PageRepository pageRepository;
    
    public static void main(String[] args)
    {
        // create a new book
        Book book = new Book("Java 101", "John Doe", "123456");

        // save the book
        bookRepository.save(book);

        // create and save new pages
        pageRepository.save(new Page(1, "Introduction contents", "Introduction", book));
        pageRepository.save(new Page(65, "Java 8 contents", "Java 8", book));
        pageRepository.save(new Page(95, "Concurrency contents", "Concurrency", book));
    }
}
