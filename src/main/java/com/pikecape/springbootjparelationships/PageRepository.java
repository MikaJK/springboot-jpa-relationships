package com.pikecape.springbootjparelationships;

import java.util.List;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.CrudRepository;

/**
 * PageRepository.
 *
 * @author Mika J. Korpela
 */
public interface PageRepository extends CrudRepository<Page, Long>
{
    List<Page> findByBook(Book book, Sort sort);
}
