package com.pikecape.springbootjparelationships;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

/**
 * SpringBootJpaApplication.
 * 
 * @author Mika J. Korpela
 */
@SpringBootApplication
public class SpringBootJpaApplication
{
    // logger.
    private static final Logger LOGGER = LoggerFactory.getLogger(SpringBootJpaApplication.class);
    
    public static void main(String[] args) {
        SpringApplication.run(SpringBootJpaApplication.class, args);
    }

    @Bean
    public CommandLineRunner mappingDemo(BookRepository bookRepository,
                                         PageRepository pageRepository) {
        return args -> {

            // create a new book
            Book book = new Book("Java 101", "John Doe", "123456");

            // save the book
            bookRepository.save(book);

            // create and save new pages
            pageRepository.save(new Page(1, "Introduction contents", "Introduction", book));
            pageRepository.save(new Page(65, "Java 8 contents", "Java 8", book));
            pageRepository.save(new Page(95, "Concurrency contents", "Concurrency", book));
            
            Book aBook = bookRepository.findByIsbn("123456");
            LOGGER.info(aBook.toString());
//            System.out.println(aBook.toString());
        };
    }
}
